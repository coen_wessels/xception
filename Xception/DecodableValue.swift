//  Copyright © 2015 Coen Wessels. All rights reserved.

/// A abstract data structure(for example JSON).
public protocol DecodableValue {
	/// The path to this value from the root of the structure.
	var path: Path { get }

	/// Casts the value to a shape Xception understands.
	func primitiveValue() throws -> Primitive<Self>
}

// Lookup
extension DecodableValue {
	/// Recursive lookup using given key path.
	public func lookup(_ keys: [String]) throws -> Self {
		return try keys.reduce(self) { try $0.lookup($1) }
	}

	/// Assumes the current value is a object and tries to return the value for given key.
	public func lookup(_ key: String) throws -> Self {
		let newPath = path.lookup(key: key)
		let object = try castToObject()

		if let value = object[key] {
			return value
		} else {
			throw DecodingError.missingKey(key: key, at: newPath)
		}
	}
}

// Get
extension DecodableValue {
	/// Pull the decoded value from the current value.
	public func get<Result: Decodable>() throws -> Result {
		return try Result(data: self)
	}

	/// Pull the value, following the given path, from the current value.
	public func get<Result: Decodable>(_ keys: [String]) throws -> Result {
		return try lookup(keys).get()
	}

	/// Pull the value, for the given key, from the current value.
	public func get<Result: Decodable>(_ key: String) throws -> Result {
		return try get([key])
	}
}

// Array
extension DecodableValue {
	/// Pull the decoded array from the current value.
	public func get<Result: Decodable>(_ ignoreNil: Bool = false) throws -> [Result] {
		return try mapArray(ignoreNil: ignoreNil) { element in
			try Result(data: element)
		}
	}

	/// Pull the decoded array, following the given path, from the current value.
	public func get<Result: Decodable>(_ keys: [String], ignoreNil: Bool = false) throws -> [Result] {
		return try lookup(keys).get(ignoreNil)
	}

	/// Pull the decoded array, for the given key, from the current value.
	public func get<Result: Decodable>(_ key: String, ignoreNil: Bool = false) throws -> [Result] {
		return try get([key], ignoreNil: ignoreNil)
	}
}
