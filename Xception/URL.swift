//  Copyright © 2016 Coen Wessels. All rights reserved.

extension URL: Decodable {
	public init<Data: DecodableValue>(data: Data) throws {
		let string: String = try data.get()

		if let url = URL(string: string) {
			self = url
		} else {
			throw DecodingError.typeMismatch(expected: "URL", actual: string, at: data.path)
		}
	}
}

import Foundation
