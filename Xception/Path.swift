//  Copyright © 2016 Coen Wessels. All rights reserved.

public struct Path: Equatable {
	public enum Component: Equatable {
		case key(String)
		case index(Int)
	}

	public init(_ components: [Component]) {
		self.components = components
	}

	public let components: [Component]

	public func lookup(key: String) -> Path {
		return lookup(component: .key(key))
	}

	public func lookup(index: Int) -> Path {
		return lookup(component: .index(index))
	}

	fileprivate func lookup(component: Component) -> Path {
		return Path(components + [component])
	}
}

// CustomStringConvertible
extension Path: CustomStringConvertible {
	public var description: String {
		return components.lazy
			.map { $0.description }
			.joined(separator: "/")
	}
}

extension Path.Component: CustomStringConvertible {
	public var description: String {
		switch self {
		case let .key(key): return key
		case let .index(index): return String(index)
		}
	}
}

// Equatable
public func == (left: Path, right: Path) -> Bool {
	return left.components == right.components
}

public func == (left: Path.Component, right: Path.Component) -> Bool {
	switch (left, right) {
	case let (.key(keyLeft), .key(keyRight)):
		return keyLeft == keyRight
	case let (.index(indexLeft), .index(indexRight)):
		return indexLeft == indexRight
	default:
		return false
	}
}
