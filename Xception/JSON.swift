//  Copyright © 2015 Coen Wessels. All rights reserved.

public struct JSON: DecodableValue {
	// Init
	public init(json data: Data, options: JSONSerialization.ReadingOptions = []) throws {
		self.data = try JSONSerialization.jsonObject(with: data, options: options)
		self.path = .init([])
	}

	public init(data: Any) {
		self.data = data
		self.path = .init([])
	}

	public init(data: Any, path: Path) {
		self.data = data
		self.path = path
	}

	// Constants
	public let data: Any
	public let path: Path

	// DecodableValueType
	public func primitiveValue() throws -> Primitive<JSON> {
		switch data {
		case let string as String:
			return .string(path, string)
		case let array as [AnyObject]:
			return .array(path, AnySequence(array.lazy.enumerated().map { JSON(data: $1, path: self.path.lookup(index: $0)) }))
		case let object as [String: AnyObject]:
			var result: [String: JSON] = [:]

			for (key, value) in object {
				result[key] = JSON(data: value, path: path.lookup(key: key))
			}

			return .object(path, result)
		case let number as NSNumber:
			return .number(path, number.doubleValue)
		default: throw DecodingError.nullValue(at: path)
		}
	}
}

import Foundation
