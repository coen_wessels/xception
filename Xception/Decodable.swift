//  Copyright © 2015 Coen Wessels. All rights reserved.

public protocol Decodable {
	init<Data: DecodableValue>(data: Data) throws
}
