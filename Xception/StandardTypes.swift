//  Copyright © 2015 Coen Wessels. All rights reserved.

extension String: Decodable {
	public init<Data : DecodableValue>(data: Data) throws {
		self = try data.castToString()
	}
}

extension Int: Decodable {
	public init<Data : DecodableValue>(data: Data) throws {
		self = try Int(data.castToNumeric(ofType: "Int"))
	}
}

extension Int64: Decodable {
	public init<Data : DecodableValue>(data: Data) throws {
		self = try Int64(data.castToNumeric(ofType: "Int64"))
	}
}

extension Double: Decodable {
	public init<Data : DecodableValue>(data: Data) throws {
		self = try data.castToDouble()
	}
}

extension Bool: Decodable {
	public init<Data : DecodableValue>(data: Data) throws {
		switch try data.castToNumeric(ofType: "Bool") {
		case 0: self = false
		case 1: self = true
		case let n: throw DecodingError.typeMismatch(expected: "Bool", actual: String(n), at: data.path)
		}
	}
}

extension Float: Decodable {
	public init<Data : DecodableValue>(data: Data) throws {
		self = try Float(data.castToNumeric(ofType: "Float"))
	}
}
