//  Copyright © 2015 Coen Wessels. All rights reserved.

#import <Foundation/Foundation.h>

//! Project version number for Xception.
FOUNDATION_EXPORT double XceptionVersionNumber;

//! Project version string for Xception.
FOUNDATION_EXPORT const unsigned char XceptionVersionString[];