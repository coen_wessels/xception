//  Copyright © 2015 Coen Wessels. All rights reserved.

/// Concrete representation of the tree structure. Xception uses this structure to cast a generic DecodableValueType to a shape it understands.
public enum Primitive<Data: DecodableValue> {
	case string(Path, String)
	case array(Path, AnySequence<Data>)
	case object(Path, [String: Data])
	case number(Path, Double)

	public var path: Path {
		switch self {
		case let .string(path, _): return path
		case let .array(path, _): return path
		case let .object(path, _): return path
		case let .number(path, _): return path
		}
	}

	/// Helpers function to construct a DecodingError.
	fileprivate func typeMismatch(expected: String) -> DecodingError {
		let actual: String
		switch self {
		case let .string(_, string): actual = "string(\(string))"
		case let .array(_, array): actual = "array(\(array))"
		case let .object(_, object): actual = "object(\(object))"
		case let .number(_, number): actual = "number(\(number))"
		}
		return .typeMismatch(expected: expected, actual: actual, at: path)
	}
}

// Cast to case
extension DecodableValue {
	public func castToArray() throws -> AnySequence<Self> {
		let primitive = try primitiveValue()

		if case let .array(_, array) = primitive {
			return array
		} else {
			throw primitive.typeMismatch(expected: "Array")
		}
	}

	public func castToObject() throws -> [String: Self] {
		let primitive = try primitiveValue()

		if case let .object(_, object) = primitive {
			return object
		} else {
			throw primitive.typeMismatch(expected: "Object")
		}
	}

	public func castToString() throws -> String {
		let primitive = try primitiveValue()

		if case let .string(_, string) = primitive {
			return string
		} else {
			throw primitive.typeMismatch(expected: "String")
		}
	}

	public func castToDouble() throws -> Double {
		return try castToNumeric(ofType: "Double")
	}

	public func castToNumeric(ofType type: String) throws -> Double {
		let primitive = try primitiveValue()

		if case let .number(_, number) = primitive {
			return number
		} else {
			throw primitive.typeMismatch(expected: type)
		}
	}
}

// Map
extension DecodableValue {
	internal func mapArray<Result>(ignoreNil: Bool = false, transform: (Self) throws -> Result) throws -> [Result] {
		let array = try castToArray()

		if ignoreNil {
			return array.flatMap { value in try? transform(value) }
		} else {
			return try array.map(transform)
		}
	}
}

import Foundation
