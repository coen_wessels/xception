//  Copyright © 2015 Coen Wessels. All rights reserved.

public enum DecodingError: Error, CustomStringConvertible {
	case missingKey(key: String, at: Path)
	case typeMismatch(expected: String, actual: String, at: Path)
	case nullValue(at: Path)

	public var description: String {
		switch self {
		case let .missingKey(key: key, at: at):
			return "missing key \(key) at \(at.description)"
		case let .typeMismatch(expected: expected, actual: actual, at: at):
			return "there is a type mismatch at \(at.description), expected \(expected) but the actual value is: \(actual)"
		case let .nullValue(at: at):
			return "unexpected null value at \(at.description)"
		}
	}
}
