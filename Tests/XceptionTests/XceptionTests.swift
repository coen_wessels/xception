//  Copyright © 2015 Coen Wessels. All rights reserved.

import XCTest
@testable import Xception

extension JSON {
	public init(json string: String, options: JSONSerialization.ReadingOptions = []) throws {
		self = try JSON(json: string.data(using: String.Encoding.utf8)!, options: options)
	}
}

public struct Person: Decodable, Equatable {
	public init(id: Int, name: String, description: String, title: String?, values: [String], boolean: Bool, otherBoolean: Bool) {
		self.id = id
		self.name = name
		self.description = description
		self.title = title
		self.values = values
		self.boolean = boolean
		self.otherBoolean = otherBoolean
	}

	public init<Data: DecodableValue>(data: Data) throws {
		id = try data.get("id")
		name = try data.get("name")
		description = try data.get(["lol", "description"])
		title = try? data.get("title")
		values = try data.get("values")
		boolean = try data.get("boolean")
		otherBoolean = try data.get("other_boolean")
	}

	public let id: Int
	public let name: String
	public let description: String
	public let title: String?
	public let values: [String]
	public let boolean: Bool
	public let otherBoolean: Bool
}

public func == (left: Person, right: Person) -> Bool {
	return left.id == right.id
		&& left.name == right.name
		&& left.description == right.description
		&& left.title == right.title
		&& left.values == right.values
		&& left.boolean == right.boolean
		&& left.otherBoolean == right.otherBoolean
}

class XceptionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

	func testJSONArray() {
		let array = "[1, 2, 3, 4, 5, 6]"

		do {
			let json = try JSON(json: array.data(using: String.Encoding.utf8)!)
			let result: [Int] = try json.get()
			XCTAssert(result == [1, 2, 3, 4, 5, 6])
		} catch {
			XCTFail("\(error)")
		}
    }

	func testJSONString() {
		do {
			let json = try JSON(json: "[\"Coen Wessels\"]")
			let string: [String] = try json.get()
			XCTAssert(string == ["Coen Wessels"])
		} catch {
			XCTFail("\(error)")
		}

		do {
			let json = try JSON(json: "[1]")
			let array: [JSON] = try Array(json.castToArray())
			let _: String = try array.first!.get()
			XCTFail("1 is not a String")
		} catch let DecodingError.typeMismatch(expected: expected, actual: actual, at: _) {
			XCTAssertEqual(expected, "String")
			XCTAssertEqual(actual, "number(1.0)")
		} catch {
			XCTFail("Error must be TypeMismatch")
		}
	}

	func testJSONObject() {
		do {
			let json = try JSON(json: "{ \"name\": \"Coen Wessels\" }")
			let string: String = try json.get("name")
			XCTAssert(string == "Coen Wessels")
		} catch {
			XCTFail("\(error)")
		}

		do {
			let json = try JSON(json: "{ \"_name\": \"Coen Wessels\" }")
			let _: String = try json.get("name")
			XCTFail("key Name does not exist")
		} catch let DecodingError.missingKey(key: key, at: _) {
			XCTAssert(key == "name")
		} catch {
			XCTFail("Expected: MissingKey(name) actual: \(error)")
		}
	}

	func testPerson() {
		do {
			let jsonString = "{"
				+ "\"name\": \"Coen Wessels\","
				+ "\"id\": 200,"
				+ "\"lol\": {"
					+ "\"description\": \"Gewoon leuk\""
				+ "},"
				+ "\"title\": null,"
				+ "\"values\": [\"hoi\", \"hoi2\"],"
				+ "\"boolean\": true,"
				+ "\"other_boolean\": false"
			+ "}"
			let json = try JSON(json: jsonString)

			let expected = Person(id: 200,
			                      name: "Coen Wessels",
			                      description: "Gewoon leuk",
			                      title: nil,
			                      values: ["hoi", "hoi2"],
			                      boolean: true,
			                      otherBoolean: false)
			let actual: Person = try Person(data: json)
			XCTAssertEqual(actual, expected)
		} catch {
			XCTFail("\(error)")
		}
	}

	func testURL() {
		let json = JSON(data: ["url": "http://google.com/"])

		do {
			let url: URL = try json.get("url")
			XCTAssertEqual(url, URL(string: "http://google.com/")!)
		} catch {
			XCTFail("\(error)")
		}
	}

	func testBoolean() {
		let json = JSON(data: ["true": true, "false": false, "not_a_bool": 100])
		XCTAssertEqual(try json.get("true"), true)
		XCTAssertEqual(try json.get("false"), false)
		XCTAssertThrowsError(try json.get("not_a_bool") as Bool)
	}
}
