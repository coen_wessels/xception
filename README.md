# Xception #

Xception is a library that transforms abstract JSON like structures to your own models. It is inspired by [Argo](https://github.com/thoughtbot/Argo) but uses the swift error handling model and initializers instead of custom operators.

## Installation

### [Carthage]

[Carthage]: https://github.com/Carthage/Carthage

Add the following to your Cartfile:

```
git "git@bitbucket.org:coen_wessels/xception.git"
```

Then run `carthage update`.

Follow the current instructions in [Carthage's README][carthage-installation]
for up to date installation instructions.

[carthage-installation]: https://github.com/Carthage/Carthage#adding-frameworks-to-an-application

### Concepts

#### DecodableValueType
Using Xception you are able to decode any JSON like structure. To decode your own abstract data structure you can implement `DecodableValueType`. The only thing you have to do is add a function that transforms your own structure to a structure Xception understands, namely: `Primitive`. You can find an example in the source code, Xception already includes a implementation for [JSON](https://bitbucket.org/coen_wessels/xception/src/91d53f23ae5eb17c71fb0372eb64e26b0724bb01/Xception/JSON.swift).

#### Decodable
**TBD**

### Example Decodable
```swift
public struct User {
	public enum Role: String {
		case admin
		case user
	}

	public let id: Int
	public let name: String
	public let email: String?
	public let role: Role
	public let companyName: String
	public let friends: [User]
}

extension User: Decodable {
	public init<Data: DecodableValueType>(data: Data) throws {
		id = try data.get("id")
		name = try data.get("name")
		email = try? data.get("email")
		role = try data.get("role")
		companyName = try data.get(["company", "role"])
		friends = try data.array("friends") // To decode an array of decodable's use the `array` function
	}
}

extension User.Role: Decodable {
	public init<Data: DecodableValueType>(data: Data) throws {
		if let role = try User.Role(rawValue: data.get()) {
			self = role
		} else {
			throw DecodingError.typeMismatch(expected: "Role", actual: "\(data)", at: try data.path())
		}
	}
}
```